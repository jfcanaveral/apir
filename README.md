# Welcome to `apir` node module

This node module offers an approach to testing REST APIs.

## Code Example (using mocha or jasmine)

```javascript
var _ = require('lodash');
var testStep = require('apir');

var data = {
   "host" : "http://some-url.com:8080",
   "method" : "POST",
   "endpoint" : "/account_types",
   "headers" : {
       "Content-Type"   : "application/json",
   },
   "requestBody" : {
       "name": "SAMPLE NAME",
       "description" : "SAMPLE DESC"
   },
   "statusCode" : 200,
   "responseBody" : {
       "success": true
   }
 };
};

describe('sample test' , function(){
    context('when data is valid', function(){
        it('should be successful', function(done){
            testStep(data, done);
        });
    });  
});
```
The module returns the response body of the API call. Hence if you want to do something with the results, you can do:
```javascript
describe('sample test' , function(){
  context('when data is valid', function(){
    it('should be successful', function(done){
      testStep(data, function(err, res){
        if(err) {
          return done(err)
        };
        //do something with results
        console.log(res.body);
        done();
      });
    });
  });
});
```

## Motivation
The idea with this approach is to represent the whole data stack using just one JSON. These are:

### host
* mandatory

### method
* optional, default is GET
* supported methods: GET, POST, PUT, DELETE
* support for other methods such as HEAD will be provided in the future

### endpoint 
* mandatory

### headers
* optional

### requestBody
* mandatory for POST, PUT, DELETE

### statusCode
* optional, no default value

### responseBody
* optional

## License

Copyright (c) 2016, James Cañaveral <james.canaveral@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.