var request = require('supertest');
var _ = require('lodash');
var expect = require('chai').expect;
var assert = require('chai').assert;

var apir = function (data, done) {
    var req = request(data.host);
    data.method = (data.method ? data.method.toUpperCase() : 'GET');
    switch (data.method) {
        case 'GET':
            req = req.get(data.endpoint);
            break;
        case 'POST':
            req = req.post(data.endpoint)
                .send(data.requestBody);
            break;
        case 'PUT':
            req = req.put(data.endpoint)
                .send(data.requestBody);
            break;
        case 'PATCH':
            req = req.patch(data.endpoint)
                .send(data.requestBody);
            break;
        case 'DELETE':
            req = req.del(data.endpoint)
                .send(data.requestBody);
            break;
        case 'COPY':
            req = req.copy(data.endpoint);
            break;
        case 'HEAD':
            req = req.head(data.endpoint);
            break;
        case 'OPTIONS':
            req = req.options(data.endpoint)
                .send(data.requestBody);
            break;
        case 'PURGE':
            req = req.purge(data.endpoint);
            break;
        case 'LOCK':
            req = req.lock(data.endpoint)
                .send(data.requestBody);
            break;
        case 'UNLOCK':
            req = req.unlock(data.endpoint);
            break;
        case 'PROPFIND':
            req = req.propfind(data.endpoint)
                .send(data.requestBody);
            break;
    }
    req = data.headers ? req.set(data.headers) : req;
    req = data.statusCode ? req.expect(data.statusCode) : req;
    req.end(function (err, res) {
        if (err) {
            return done(err, res);
        } else {
            if (!!data.responseBody) {
                // assert only if lodash function isMatch(actual, expected) returns false
                if (!_.isMatch(res.body, data.responseBody)) {
                    assert.deepEqual(res.body, data.responseBody, 'expected is not a subset of the actual');
                }
            }

            done(null, res);
        }
    });
};

module.exports = apir;
